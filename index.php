<?php

function calculateMonthlyPayment($totalLoan, $interestRate, $years) {
    $monthlyInterestRate = $interestRate / 100 / 12;
    $numberOfPayments = $years * 12;
    $monthlyPayment = $totalLoan * $monthlyInterestRate * pow(1 + $monthlyInterestRate, $numberOfPayments)
        / (pow(1 + $monthlyInterestRate, $numberOfPayments) - 1);
    return $monthlyPayment;
}

$fileName = "customers.txt"; // assuming customers' information is stored in this file
$prospectNumber = 1;
$file = fopen($fileName, "r");
if ($file) {
    while (($line = fgets($file)) !== false) {
        if($prospectNumber <= 4){
            $tokens = explode(",", $line);
            $customerName = trim($tokens[0]);
            $totalLoan = (float)trim($tokens[1]);
            $interestRate = (float)trim($tokens[2]);
            $years = (int)trim($tokens[3]);
            $monthlyPayment = calculateMonthlyPayment($totalLoan, $interestRate, $years);
            echo "<pre>";
            echo "Prospect $prospectNumber: $customerName wants to borrow $totalLoan € with Interest rate of $interestRate for a period of $years years and pay ".round($monthlyPayment,2)." € each month\n";
            echo "<pre>";
            $prospectNumber++;
        }
        
    }
    fclose($file);
} else {
    echo "Error reading file.";
}
?>
